'''
Created on May 11, 2016

@author: asarker
'''
''' 
Standard imports
'''
import re, string, os, operator,math
from collections import defaultdict

try:
    import ujson as json
except ImportError:
    import json


'''
Initialize objects
'''
english_to_american = {}
lexnorm_oovs = []
IGNORE_LIST_TRAIN = []
IGNORE_LIST = []

#---------------------Step 1: text preprocessing functions..---------------------------------------------#se
def preprocessText(tokens, IGNORE_LIST, ignore_username=True, ignore_hashtag=True, ignore_repeated_chars=False, eng_to_am=True, ignore_urls=True):
    '''
    Note the reason it ignores hashtags, @ etc. is because there is a preprocessing technique that is 
    designed to remove them 
    '''
    #global IGNORE_LIST
    normalized_tokens =[]
    #print 'Tokens passed on to preprocessing...'
    #print tokens
    text_string = ''
    # NOTE: if nesting if/else statements, be careful about execution sequence...
    for t in tokens:
        t_lower = t.strip().lower()
        # if the token is not in the IGNORE_LIST, do various transformations (e.g., ignore usernames and hashtags, english to american conversion
        # and others..
        if t_lower not in IGNORE_LIST:
            # ignore usernames '@'
            if re.match('@', t) and ignore_username:
                IGNORE_LIST.append(t_lower)
                text_string += t_lower + ' '
            # ignore hashtags
            elif re.match('#', t_lower) and ignore_hashtag:
                IGNORE_LIST.append(t_lower)
                text_string += t_lower + ' '
            # convert english spelling to american spelling
            elif t.strip().lower() in english_to_american.keys() and eng_to_am:    
                text_string += english_to_american[t.strip().lower()] + ' '
            #URLS
            elif re.search('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', t_lower) and ignore_urls:
                IGNORE_LIST.append(t_lower)
                text_string += t_lower + ' '                
            elif not ignore_repeated_chars and not re.search(r'[^a-zA-Z]', t_lower):
                # if t_lower only contains alphabetic characters
                t_lower = re.sub(r'([a-z])\1+', r'\1\1', t_lower)
                text_string += t_lower + ' '  
                # print t_lower
                
            # if none of the conditions match, just add the token without any changes..
            else:
                text_string += t_lower + ' '
        else:  # i.e., if the token is in the ignorelist..
            
            text_string += t_lower + ' '
        normalized_tokens = text_string.split()
    # print normalized_tokens
    return normalized_tokens, IGNORE_LIST

#---------------------Step 2: dictionary based normalization---------------------------------------------#
def dictionaryBasedNormalization(tokens, I_LIST, M_LIST):
    for t in tokens:
        t_lower = t.strip().lower()
        if t_lower in noslang_dict.keys() and len(t_lower)>2:
            nt = noslang_dict[t_lower]
            tokens[tokens.index(string.strip(t))] = nt
            if not t_lower in M_LIST:
                M_LIST.append(t_lower)
            if not nt in M_LIST:
                M_LIST.append(nt)
    return tokens, I_LIST, M_LIST

#-------------------LOADER FUNCTIONS----------------------------------------#
def loadItems():
    '''
    This is the primary load function.. calls other loader functions as required..
    '''
    global one2subs
    global english_to_american
    global noslang_dict
    global IGNORE_LIST_TRAIN
    
    one2subs = loadone2subs()
    english_to_american = loadEnglishToAmericanDict()
    noslang_dict = loadDictionaryData()
    
    return None
    
                
        
def loadEnglishToAmericanDict():
    etoa = {}
    
    english = open('englishspellings')
    american = open('americanspellings')
    for line in english:
        etoa[line.strip()] = american.readline().strip()
    return etoa

def loadDictionaryData():
    '''
    this function loads the various dictionaries which can be used for mapping from oov to iv
    '''
    n_dict = {}
    infile = open('noslang_mod.txt')
    for line in infile:
        items = line.split(' - ')
        if len(items[0]) > 0 and len(items) > 1:
            n_dict[string.strip(items[0])] = string.strip(items[1])
    return n_dict

   

def loadone2subs():
    subs = {}
    infile = open('1_2letter_words')
    for line in infile:
        items = string.strip(line).split('\t')
        if len(items)>=3:
            org_word = items[0]
            sublist = items[2:]
            subs[org_word] = sublist
    return subs

'''
This is the primary normalize function... this sequentially calls all the other functions.
'''
def normalize(tokens, IGNORE_LIST,parameter_threshold,sp,oovoutfile=None):
    MOD_LIST = []
    # Step 1: preprocess the text
    normalized_tokens, il = preprocessText(tokens, IGNORE_LIST)
    normalized_text = ' '.join(normalized_tokens)
    normalized_minus_ignorelist = [t for t in normalized_tokens if t not in IGNORE_LIST]
    # lowercase tokens..
    normalized_tokens = [str(t.lower()) for t in normalized_tokens]
    normalized_text = ' '.join(normalized_tokens)
    # dictionary based normalization
    ml = MOD_LIST
    normalized_tokens, il, ml = dictionaryBasedNormalization(normalized_tokens, il, ml)
    return normalized_tokens, normalized_text
